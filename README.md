# Sorta Sticky Runners

This project has shared runners disabled.

It also has 2 runners registered, one called `sticky-0` and one called `sticky-1`. The two "sticky" runners each have tags matching those names. They can also run untagged jobs.

## How's it work

The first job in the pipeline is a prep job. It chooses a sticky runner to stick with for every other job. As a proof of concept, it just takes the Pipeline IID and does modular arithmetic on it against the number of sticky runners registered to the project (2). This has some implications that kind of break the scheduling algorithm by ignoring load and assignign the rest of the sticky jobs to this arbitrarily chosen runner. A more sophisticated version could use the GitLab API and query the tag associated with the prep job (assuming it was a sticky runner) and then keep all subsequent jobs on that runner - at least this way we know that that runner was free to run jobs when it picked up the prep job. In this PoC version the chosen sticky runner might be bogged down so keep that in mind.

Once the sticky runner is chosen, the actual pipeline definition in `pipeline.yml` is edited to substitute the value of `STICKYTAG` for one the actual sticky tag of the chosen runner (ie: `sticky-0` or `sticky-1`). `pipeline.yml` is then triggered as the remaining parts of the pipeline. All jobs wanting to be stuck to a consistent runner are, and jobs that have no sticky-affinity are free to execute on any runner that they match with.

Here's a pipeline [where all jobs stuck to sticky-1](https://gitlab.com/erushton/sorta-sticky-runners-poc/-/pipelines/417435036) and here's a pipeline where [all jobs stuck to sticky-0](https://gitlab.com/erushton/sorta-sticky-runners-poc/-/pipelines/417435038)

### Some benefits

* You can have multiple "sticky" environments in a given pipeline. Not all sticky jobs have to co-habit the same runner.
* You can opt out of stickyness for jobs that aren't improved by this.
* The pipeline is still defined in a lintable yaml file and other than the sticky tags needing to be substituted is a completely valid GitLab CI definition.
* It already works. All the features this uses have been in GitLab for several releases.
